from requests import exceptions
from stravalib.client import Client


def get_client(token):
    client = Client(access_token=token)
    return client


def get_athelete(token):
    client = get_client(token)
    athlete = client.get_athlete()
    items = {}
    items['stats'] = {}
    items['stats']['recent_ride_totals'] = {}
    items['stats']['ytd_ride_totals'] = {}
    items['stats']['all_ride_totals'] = {}
    items['id'] = athlete.id
    items['first_name'] = athlete.firstname
    items['last_name'] = athlete.lastname
    items['stats']['biggest_ride_distance'] = athlete.stats.biggest_ride_distance.num
    items['stats']['biggest_climb_elevation_gain'] = athlete.stats.biggest_climb_elevation_gain
    items['stats']['recent_ride_totals'] = {
        'count': athlete.stats.recent_ride_totals.count,
        'achievement_count': athlete.stats.recent_ride_totals.achievement_count,
        'distance': athlete.stats.recent_ride_totals.distance.num,
        'elapsed_time': athlete.stats.recent_ride_totals.elapsed_time.seconds,
        'elevation_gain': athlete.stats.recent_ride_totals.elevation_gain.num,
        'moving_time': athlete.stats.recent_ride_totals.moving_time.seconds,
    }
    items['stats']['ytd_ride_totals'] = {
        'count': athlete.stats.ytd_ride_totals.count,
        'distance': athlete.stats.ytd_ride_totals.distance.num,
        'elapsed_time': athlete.stats.ytd_ride_totals.elapsed_time.seconds,
        'elevation_gain': athlete.stats.ytd_ride_totals.elevation_gain.num,
        'moving_time': athlete.stats.ytd_ride_totals.moving_time.seconds,
    }
    items['stats']['all_ride_totals'] = {
        'count': athlete.stats.all_ride_totals.count,
        'distance': athlete.stats.all_ride_totals.distance.num,
        'elapsed_time': athlete.stats.all_ride_totals.elapsed_time.seconds,
        'elevation_gain': athlete.stats.all_ride_totals.elevation_gain.num,
        'moving_time': athlete.stats.all_ride_totals.moving_time.seconds,
    }
    return items


def get_activities(token):
    client = get_client(token)
    activities = []
    for item in client.get_activities():
        item = client.get_activity(item.id, include_all_efforts=True)
        items = {
            'activity_id': item.id,
            'achievement_count': item.achievement_count,
            'average_speed': item.average_speed.num,
            'average_watts': item.average_watts,
            'distance': item.distance.num,
            'elapsed_time': item.elapsed_time.seconds,
            'kilojoules': item.kilojoules,
            'max_speed': item.max_speed.num,
            'max_watts': item.max_watts,
            'moving_time': item.moving_time.seconds,
            'name': item.name,
            'pr_count': item.pr_count,
            'start_date_local': item.start_date_local.date(),
            'total_elevation_gain': item.total_elevation_gain.num,
            'weighted_average_watts': item.weighted_average_watts,
        }
        activities.append(items)
    return activities


def get_segments(token):
    client = get_client(token)
    activity_ids = [activity.id for activity in client.get_activities()]
    items = []
    for activity_id in activity_ids:
        item = client.get_activity(activity_id, include_all_efforts=True)
        segment_efforts = item.segment_efforts
        if segment_efforts:
            for segment_effort in segment_efforts:
                leaderboard = []
                segment = client.get_segment_effort(segment_effort.id).segment
                athlete_segment = {
                    'average_grade': segment.average_grade,
                    'distance': segment.distance.num,
                    'elevation_high': segment.elevation_high.num,
                    'elevation_low': segment.elevation_low.num,
                    'maximum_grade': segment.maximum_grade,
                    'name': segment.name,
                }
                segment_id = segment.id
                top_results_limit = 200
                entry_count = client.get_segment_leaderboard(
                    segment_id,
                    top_results_limit=200
                ).entry_count
                if(divmod(entry_count, top_results_limit)[1] == 0):
                    page = divmod(entry_count, top_results_limit)[0]
                    pages = range(1, page + 1)
                else:
                    page = divmod(entry_count, top_results_limit)[0] + 1
                    pages = range(1, page + 1)
                for page in pages:
                    leader_board = client.get_segment_leaderboard(
                        segment_id,
                        top_results_limit=200,
                        page=page
                    )
                    entry_list = leader_board.entries
                    if entry_list != []:
                        for entry in entry_list:
                            lb_details = {
                                'athlete_gender': entry.athlete_gender,
                                'athlete_name': entry.athlete_name,
                                'average_watts': entry.average_watts,
                                'distance': entry.distance.num,
                                'elapsed_time': entry.elapsed_time.seconds,
                                'moving_time': entry.moving_time.seconds,
                                'rank': entry.rank,
                                'start_date_local': entry.start_date_local.date(),
                            }
                            leaderboard.append(lb_details)
                        items.append({
                            'segment': athlete_segment,
                            'leaderboard': leaderboard,
                        })
                    else:
                        break
    return items


def get_streams(token):
    client = get_client(token)
    activity_ids = [activity.id for activity in client.get_activities()]
    types = ['latlng']
    items = []
    for activity_id in activity_ids:
        try:
            stream = client.get_activity_streams(
                activity_id,
                types,
                resolution='high'
            )
            streams = stream['latlng']
            items.append({
                'lat': streams.data[0][0],
                'lng': streams.data[1][1],
            })
        except exceptions.HTTPError:
            continue
    return items
